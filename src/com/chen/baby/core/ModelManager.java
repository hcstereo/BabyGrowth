package com.chen.baby.core;

/**
 * Model管理类,通过该类可以根据主键获取任意Model的实体类
 * @author huachongchen
 *
 */
public class ModelManager {
    private static ModelManager mModelManager;
    private ModelManager(){
        
    }
    public static ModelManager getInstance(){
        synchronized (mModelManager) {
            if(mModelManager==null){
                mModelManager=new ModelManager();
            }
            return mModelManager;
        }
    }
}
