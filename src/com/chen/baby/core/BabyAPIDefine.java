package com.chen.baby.core;

import com.chen.baby.BuildConfig;

public final class BabyAPIDefine {
    public static final String DO = "do";
    public static final String CLIENT_ANDROID = "android";
    public static final String API_VER = "4";
    
    public static final String SCHEMA = "http";
    
    public static final String HOST_API_RELEASE = "api.club.sohu.com";
    public static final String HOST_API_DEBUG = HOST_API_RELEASE;
    public static final String HOST_API = BuildConfig.DEBUG ? HOST_API_DEBUG : HOST_API_RELEASE;// 接口
    
    public static final String KEY_ERROR_CODE = "errorCode";
}
