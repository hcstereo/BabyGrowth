package com.chen.baby.core;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import android.app.Application;
import android.util.Log;

/**
 * Application单例
 * 
 * @author huachongchen
 *
 */
public class AppController extends Application {
    private final static String TAG = AppController.class.getSimpleName();
    private static AppController mAppController;
    private RequestQueue mRequestQueue;// 请求队列
    private ImageLoader mImageLoader;

    private AppController() {

    }

    public synchronized static AppController getInstance() {
        if (mAppController == null) {
            mAppController = new AppController();
        }
        return mAppController;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "oncreate()");
    }

    /**
     * 获取请求队列
     * 
     * @return
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(this);
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader=new ImageLoader(getRequestQueue(), new LruBitmapCache());
        }
        return mImageLoader;
    }
    public <T> void addToRequestQueue(Request<T> req,String tag){
        req.setTag(tag);
        getRequestQueue().add(req);
    }
    public <T> void addToRequestQueue(Request<T> req){
        req.setTag(TAG);
        getRequestQueue().add(req);
    }
    public void cancelPendingRequests(String tag){
        if(mRequestQueue!=null){
            mRequestQueue.cancelAll(tag);
        }
    }
}
