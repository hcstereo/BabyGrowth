package com.chen.baby.core;

import java.util.HashSet;

import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.android.volley.Response.Listener;
import com.chen.baby.info.LoginInfo;
import com.chen.baby.model.UserInfo;
import com.chen.baby.request.LoginRequest;
import com.chen.baby.ui.NetWorkHandler;

/**
 * 账户管理Manager，用于判断用户是否登录，获取用户信息等
 * 
 * @author huachongchen
 *
 */
public class AccountManager {
    private static final String TAG = AccountManager.class.getSimpleName();
    private static AccountManager mAccountManager;

    private AccountManager() {

    }
    public interface AccountActionListener{
        void onAccountAction(Action action);
    }
    public static enum Action {
        ACCOUNT_USER_SWITCH, ACCOUNT_LOGOUT, ACCOUNT_LOGIN_SUCCESS, ACCOUNT_LOGIN_FAILED, ACCOUNT_LOGIN_WORKING, FETCH_USER_INFO_COMPLETE, FETCH_FORUMS_START, FETCH_FORUMS_COMPLETE, FETCH_FORUMS_FAILED
    }
    private HashSet<AccountActionListener> mAccountActionListeners=new HashSet<AccountManager.AccountActionListener>();
    public static AccountManager getInstance() {
        synchronized (mAccountManager) {
            if (mAccountManager == null) {
                mAccountManager = new AccountManager();
            }
            return mAccountManager;
        }

    }

    /**
     * 根据用户明获得用户信息
     * 
     * @param username
     * @return
     */
    public UserInfo getUserInfo(String username) {
        return null;
    }

    /**
     * 判断用户是否登录
     * 
     * @return
     */
    public boolean isUserLogin() {
        return false;
    }

    /**
     * login
     * 
     * @param username
     * @param password
     */
    public void doLogin(String username, String password) {
        LoginInfo info = new LoginInfo();
        info.setLoginName(username);
        info.setPasswordMd5(password);
        LoginRequest loginRequest=new LoginRequest(null, new Listener<LoginInfo>() {
            @Override
            public void onResponse(LoginInfo response) {
                onLoginFinish(response.ERROR_CODE.isOK());
            }
        }, null,info);
        AppController.getInstance().addToRequestQueue(loginRequest);
    }
    /**
     * 登录结束
     * @param loginResult
     */
    public void onLoginFinish(boolean loginResult){
        new AsyncTask<String, Void, Action>() {
            @Override
            protected Action doInBackground(String... params) {
                if(params.length>0 && !TextUtils.isEmpty(params[0])){
                    boolean loginResult=Boolean.parseBoolean(params[0]);
                    if(loginResult){
                        return Action.ACCOUNT_LOGIN_SUCCESS;
                    }
                }
                return Action.ACCOUNT_LOGIN_FAILED;
            }
            @Override
            protected void onPostExecute(Action result) {
                notifyActionListeners(result);
            }
        }.execute(Boolean.toString(loginResult));
    }
    public void registerActionListener(AccountActionListener listener){
        this.mAccountActionListeners.add(listener);
    }
    public void unRegisterActionListener(AccountActionListener listener){
        this.mAccountActionListeners.remove(listener);
    }
    private void notifyActionListeners(Action action){
        for(AccountActionListener listener:mAccountActionListeners){
            listener.onAccountAction(action);
        }
    }
}
