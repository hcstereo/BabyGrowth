package com.chen.baby.ui;

import com.chen.baby.R;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

/**
 * Baby项目Activity基类
 * @author huachongchen
 *
 */
public class BabyBaseActivity extends BaseActivity {
    private static final String TAG=BabyBaseActivity.class.getSimpleName();
    private ProgressDialog mProgressDialog;
    //以下打印日志，记录Activity生命周期
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "execute onCreate()");
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "execute onStart()");
    }
    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "execute onResume()");
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "execute onPause()");
    }
    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "execute onStop()");
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "execute onDestroy()");
    }
    
    /**
     * 对于耗时操作显示对话框
     */
    protected void showProgressDialog() {
        if(mProgressDialog==null){
            mProgressDialog=ProgressDialog.show(this, getResString(R.string.base_dialog_title), getResString(R.string.base_dialog_content));
        }
    }
    /**
     * 关闭对话框
     */
    protected void hideProgressDialog() {
        if(mProgressDialog.isShowing()){
            mProgressDialog.cancel();
        }
    }
    /**
     * 根据资源Id返回String
     * @param resorceId
     * @return
     */
    protected String getResString(int resorceId) {
        return getResources().getString(resorceId);
    }
    /**
     * 显示short toast
     * @param text
     */
    protected void makeShortToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
    }
    /**
     * 显示long toast
     * @param text
     */
    protected void makeLongToast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }
}
