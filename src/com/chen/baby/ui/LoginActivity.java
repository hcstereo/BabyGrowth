package com.chen.baby.ui;

import com.chen.baby.BuildConfig;
import com.chen.baby.R;
import com.chen.baby.R.id;
import com.chen.baby.R.layout;
import com.chen.baby.R.menu;
import com.chen.baby.core.AccountManager;
import com.chen.baby.core.AccountManager.AccountActionListener;
import com.chen.baby.core.AccountManager.Action;
import com.chen.baby.core.BabyConst;

import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 用户登录
 * 
 * @author huachongchen
 *
 */
public class LoginActivity extends BabyBaseActivity implements OnClickListener , AccountActionListener{
    private EditText mEdtUsername;
    private EditText mEdtPassword;
    private String mUsername;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountManager.getInstance().registerActionListener(this);
        setContentView(R.layout.activity_login);
        mEdtUsername = (EditText) findViewById(R.id.edt_login_username);
        mEdtPassword = (EditText) findViewById(R.id.edt_login_password);
        ((Button) findViewById(R.id.btn_login_submit)).setOnClickListener(this);
        ((Button) findViewById(R.id.btn_login_register))
                .setOnClickListener(this);
        if (BuildConfig.DEBUG) {// 调试时自动填充用户名密码
            mEdtUsername.setText(BabyConst.USERNAME);
            mEdtPassword.setText(BabyConst.PASSWORD);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.btn_login_submit:// 点击登录按钮
            mUsername = mEdtUsername.getText().toString().trim();
            mPassword = mEdtPassword.getText().toString().trim();
            if (!isInputValid(mUsername, mPassword)) {
                break;
            }
            showProgressDialog();
            AccountManager.getInstance().doLogin(mUsername, mPassword);
        case R.id.btn_login_register:

            break;
        default:
            break;
        }
    }

    /**
     * 判断用户名和密码是否合法
     * 
     * @param username
     * @param password
     * @return
     */
    private boolean isInputValid(String username, String password) {
        if (TextUtils.isEmpty(username)) {
            makeShortToast(getResString(R.string.login_null_username));
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            makeShortToast(getResString(R.string.login_null_password));
            return false;
        }
        return true;
    }
    /**
     * 登录成功
     */
    public void onLoginSuccess(){
        hideProgressDialog();
        makeShortToast(getString(R.string.login_success));
    }
    /**
     * 登录失败
     */
    public void onLoginFail(){
        hideProgressDialog();
        makeShortToast(getString(R.string.login_fail));
    }

    @Override
    public void onAccountAction(Action action) {
        switch (action) {
        case ACCOUNT_LOGIN_SUCCESS:
            onLoginSuccess();
            break;
        case ACCOUNT_LOGIN_FAILED:
            onLoginFail();
        default:
            break;
        }
    }
}
