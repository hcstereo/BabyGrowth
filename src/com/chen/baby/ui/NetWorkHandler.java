package com.chen.baby.ui;

/**
 * 进行网络请求回调
 * @author huachongchen
 * @param <T>
 */
public interface NetWorkHandler<T> {
    public enum RequestEvent{
        START,
        PROGRESSING,
        FINISH
    }
    public void handleEvent(RequestEvent event,T t);
}
