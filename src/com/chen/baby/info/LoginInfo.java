package com.chen.baby.info;

import java.util.HashMap;

import com.chen.baby.core.BabyAPIDefine;

/**
 * 用于网络请求，登陆时所用的info
 * @author huachongchen
 *
 */
public class LoginInfo extends BaseBabyRequestInfo{
    private String loginName;
    private String retrunPassport;
    private String passwordMd5;
    private String gid;
    private String sig;
    private String token;
    private String ppToken;
    private ErrorCode errorCode;
    public String getLoginName() {
        return loginName;
    }
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }
    public String getRetrunPassport() {
        return retrunPassport;
    }
    public void setRetrunPassport(String retrunPassport) {
        this.retrunPassport = retrunPassport;
    }
    public String getPasswordMd5() {
        return passwordMd5;
    }
    public void setPasswordMd5(String passwordMd5) {
        this.passwordMd5 = passwordMd5;
    }
    public String getGid() {
        return gid;
    }
    public void setGid(String gid) {
        this.gid = gid;
    }
    public String getSig() {
        return sig;
    }
    public void setSig(String sig) {
        this.sig = sig;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getPpToken() {
        return ppToken;
    }
    public void setPpToken(String ppToken) {
        this.ppToken = ppToken;
    }
    @Override
    protected void fillQueryParamters(HashMap<String, String> paramters) {
        paramters.put(BabyAPIDefine.DO, "login");
    }
    @Override
    public int getRequestMethod() {
        return Method.POST;
    }
    public ErrorCode getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
    
}
