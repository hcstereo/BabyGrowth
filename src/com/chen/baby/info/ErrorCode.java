package com.chen.baby.info;

public class ErrorCode {
    public static final int CODE_OK = 0;
    public static final int CODE_UNKNOW = -1;
    private int mCode;
    private Status mStatus;
    private String mDescription;
    private ErrorCode mCauseBy;
    private String mDyingMsg;
    public boolean codeIsOk(){
        switch (mStatus) {
        case OK:
            return true;
        default:
            break;
        }
        return false;
    }
    public static enum Status {
        OK, FRIENDLY_INFO, FRIENDLY_INFO_BY_LOCAL, ERROE_BY_CALL, ERROE_BY_API, ERROE_BY_LOCAL, ERROR_BY_NETWORK, ERROE_UNKNOW
    }
    public int getCode() {
        return mCode;
    }
    public void setCode(int code) {
        this.mCode = code;
    }
    public String getDescription() {
        return mDescription;
    }
    public void setDescription(String description) {
        this.mDescription = description;
    }
    public String getDyingMsg() {
        return mDyingMsg;
    }
    public void setDyingMsg(String dyingMsg) {
        this.mDyingMsg = dyingMsg;
    }
    public boolean isOK() {
        switch (mStatus) {
        case OK:
            return true;
        default:
            return false;
        }
    }
}
