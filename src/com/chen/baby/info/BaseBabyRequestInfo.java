package com.chen.baby.info;

import java.util.HashMap;
import java.util.Map.Entry;

import android.net.Uri;

import com.chen.baby.core.BabyAPIDefine;
/**
 * 该抽象类用于拼写请求基本字符串
 * @author huachongchen
 *
 */
public abstract class BaseBabyRequestInfo extends BaseRequestInfo{

    @Override
    protected String buildRequestUrl() {
        Uri uri;
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(BabyAPIDefine.SCHEMA);
        builder.encodedAuthority(BabyAPIDefine.HOST_API);
        builder.path("/");
        HashMap<String, String> paramters = new HashMap<String, String>();
        fillQueryParamtersInternal(paramters);
        for (Entry<String, String> entry : paramters.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }
        uri = builder.build();
        return uri.toString();
    }
    protected abstract void fillQueryParamters(HashMap< String, String> paramters);
    
    protected void fillQueryParamtersInternal(HashMap< String, String> paramters){
        paramters.put("client", BabyAPIDefine.CLIENT_ANDROID);
        paramters.put("api_ver", BabyAPIDefine.API_VER);
        fillQueryParamters(paramters);
    }
}
