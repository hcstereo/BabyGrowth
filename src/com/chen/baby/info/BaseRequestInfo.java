package com.chen.baby.info;

import com.android.volley.Request;
import android.text.TextUtils;

public abstract class BaseRequestInfo {
    private String mRequestUrl;
    public interface Method extends Request.Method{
        
    }
    public final ErrorCode ERROR_CODE = new ErrorCode();
    public String getRequestUrl(){
        if(TextUtils.isEmpty(mRequestUrl)){
            mRequestUrl=buildRequestUrl();
        }
        return mRequestUrl;
    }
    protected abstract String buildRequestUrl();
    protected abstract int getRequestMethod();
    
}
