package com.chen.baby.request;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import com.chen.baby.core.BabyAPIDefine;
import com.chen.baby.info.ErrorCode;
import com.chen.baby.info.LoginInfo;

public class LoginRequest extends JsonRequest<LoginInfo> {
    private LoginInfo mLoginInfo;

    public LoginRequest(int method, String url, String requestBody,
            Listener<LoginInfo> listener, ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
    }

    public LoginRequest(String requestBody,
            Listener<LoginInfo> listener, ErrorListener errorListener,
            LoginInfo info) {
        this(info.getRequestMethod(), info.getRequestUrl(), requestBody, listener, errorListener);
        mLoginInfo = info;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put(mLoginInfo.getLoginName(), "username");
        params.put(mLoginInfo.getPasswordMd5(), "password");
        return params;
    }

    @Override
    protected Response<LoginInfo> parseNetworkResponse(NetworkResponse response) {
        parseResponse(response);
        return Response.success(mLoginInfo,
                HttpHeaderParser.parseCacheHeaders(response));
    }

    private void parseResponse(NetworkResponse response) {
        String responseStr = null;
        try {
            responseStr = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            mLoginInfo.ERROR_CODE.setCode(ErrorCode.CODE_UNKNOW);
            mLoginInfo.ERROR_CODE
                    .setDescription("cause by parse body to String");
            mLoginInfo.ERROR_CODE
                    .setDyingMsg(mLoginInfo.ERROR_CODE.getDyingMsg()
                            + String.format(
                                    "\n handle a throwable \n NetworkResponse = %s \n throwable = %s",
                                    response, e));
        }
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(responseStr);
            parseJsonResponse(jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected Response<LoginInfo> createNormalResponse(NetworkResponse response) {
        return Response.success(mLoginInfo,
                HttpHeaderParser.parseCacheHeaders(response));
    }

    protected void parseJsonResponse(JSONObject response) throws JSONException {
        mLoginInfo.ERROR_CODE.setDyingMsg(mLoginInfo.ERROR_CODE.getDyingMsg()
                + response);
        mLoginInfo.ERROR_CODE.setCode(response
                .getInt(BabyAPIDefine.KEY_ERROR_CODE));
        if (mLoginInfo.ERROR_CODE.codeIsOk()) {
            JSONObject joRes = response.getJSONObject("res");
            mLoginInfo.setLoginName(joRes.getString("username"));
            mLoginInfo.setRetrunPassport(joRes.getString("passport"));
        }
    }

    public LoginInfo getLoginInfo() {
        return mLoginInfo;
    }

    public void setLoginInfo(LoginInfo loginInfo) {
        this.mLoginInfo = loginInfo;
    }
    
}
